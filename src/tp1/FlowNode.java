package tp1;

import java.util.ArrayList;

public class FlowNode extends Node{
	
	String attrName;
	int attrId;
	ArrayList<Node> Connectible;
	ArrayList<Node> Connections;

	public FlowNode(String attrName, int attrId) {
		this.attrName = attrName;
		this.attrId = attrId;
		Connectible = new ArrayList<Node>();
		Connections = new ArrayList<Node>();
	}
	
	public void AddConnectible(Connectibles newConnectible) {
		Connectible.add(newConnectible);
	}
	
	public void AddConnection(Connection newConnection) {
		Connections.add(newConnection);
	}
	
	public void AddTo(to newTo) {

		Connection lastTo = (Connection)Connections.get(Connections.size());
		lastTo.AddTo(newTo);
	}
	

	@Override
	public String WriteToJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String WriteToXML() {
		// TODO Auto-generated method stub
		return null;
	}

}
