package tp1;

import java.util.ArrayList;

public class SystemNode extends Node{

	ArrayList<Node> Flow;
	String attrName;
	int attrId;
	String attrType;

	public SystemNode(String Name,String attrName, int attrId, String attrType) {
		Flow = new ArrayList<Node>();
		this.NodeName = Name;
		this.attrName = attrName;
		this.attrId = attrId;
		this.attrType = attrType;
	}
	
	public void AddConnectible(Connectibles newConnectible) {
		
		FlowNode lastFlow = (FlowNode)Flow.get(Flow.size());
		lastFlow.AddConnectible(newConnectible);
	}
	
	public void AddConnection(Connection newConnection) {
		
		FlowNode lastFlow = (FlowNode)Flow.get(Flow.size());
		lastFlow.AddConnection(newConnection);
	}
	
	public void AddTo(to newTo) {
		
		FlowNode lastFlow = (FlowNode)Flow.get(Flow.size());
		lastFlow.AddTo(newTo);
	}
	
	
	public void AddFlow(FlowNode newFlow) {
		
		Flow.add(newFlow);
	}
	

	@Override
	public String WriteToJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String WriteToXML() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
