package tp1;

import javax.xml.parsers.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class CustomHandler extends DefaultHandler {

	private MainBody mainBody;

	@Override
	public void startElement(String namespaceURI, String lName, String qName, Attributes attrs) {

		switch (qName) {
		case "MainBody":
			mainBody = new MainBody(attrs.getValue("bodyName"), Integer.parseInt(attrs.getValue("bodyID")));
			break;
		case "System":
			mainBody.AddSystem(new SystemNode(qName,attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")), attrs.getValue("type")));
			break;
		case "Flow":
			mainBody.AddFlow(new FlowNode(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "Atrium":
			mainBody.AddConnectible(new Atrium(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "Ventricle":
			mainBody.AddConnectible(new Ventricle(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "Artery":
			mainBody.AddConnectible(new Artery(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("startRadius")),Float.parseFloat(attrs.getValue("endRadius")),Float.parseFloat(attrs.getValue("length"))));
			break;
		case "Vein":
			mainBody.AddConnectible(new Vein(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("startRadius")),Float.parseFloat(attrs.getValue("endRadius")),Float.parseFloat(attrs.getValue("length"))));
			break;
		case "Capillaries":
			mainBody.AddConnectible(new Capillaries(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("volume")),Float.parseFloat(attrs.getValue("length"))));
			break;
		case "Nose":
			mainBody.AddConnectible(new Nose(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "AirConnectible":
			mainBody.AddConnectible(new AirConnectible(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("startRadius")),Float.parseFloat(attrs.getValue("endRadius")),Float.parseFloat(attrs.getValue("length"))));
			break;
		case "DigestiveTract":
			mainBody.AddConnectible(new DigestiveTract(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("length")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "DuodenumTract":
			mainBody.AddConnectible(new DuodenumTract(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("length")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "StomachTract":
			mainBody.AddConnectible(new StomachTract(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("length")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "RectumTract":
			mainBody.AddConnectible(new RectumTract(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("length")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "BiDuct":
			mainBody.AddConnectible(new BiDuct(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "Duct":
			mainBody.AddConnectible(new Duct(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "DuctOverflowableJunction":
			mainBody.AddConnectible(new DuctOverflowableJunction(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "DeversingDuct":
			mainBody.AddConnectible(new DeversingDuct(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "Alveoli":
			mainBody.AddConnectible(new Alveoli(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "InnerGallbladder":
			mainBody.AddConnectible(new InnerGallbladder(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id"))));
			break;
		case "SalivaryDuct":
			mainBody.AddConnectible(new SalivaryDuct(attrs.getValue("name"), Integer.parseInt(attrs.getValue("id")),Float.parseFloat(attrs.getValue("length")),Float.parseFloat(attrs.getValue("volume"))));
			break;
		case "Connection":
			mainBody.AddConnection(new Connection(qName,Integer.parseInt(attrs.getValue("id"))));
			break;
		case "to":
			mainBody.AddTo(new to(Integer.parseInt(attrs.getValue("id"))));
			break;
		case "Organ":
			mainBody.AddOrgan(new Organ(attrs.getValue("name"),Integer.parseInt(attrs.getValue("id")),Integer.parseInt(attrs.getValue("systemID"))));
			break;

		}
	}

}
