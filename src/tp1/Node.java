package tp1;

public abstract class Node {
	
	protected String NodeName;
	
	public abstract String WriteToJson();
	public abstract String WriteToXML();

}
