package tp1;

import java.util.ArrayList;

public class MainBody extends Node{
	
	String BodyName;
	int BodyId;
	ArrayList<Node> SystemList;
	ArrayList<Node> OrganList;
	
	MainBody(String _Name, int _id)
	{
		BodyName = _Name;
		BodyId = _id;
	}
	
	public void AddFlow (FlowNode newFlow) {		
		SystemNode lastSystem = (SystemNode)SystemList.get(SystemList.size());
		lastSystem.AddFlow(newFlow);
	}
	
	public void AddSystem(SystemNode newSystem) {
		SystemList.add(newSystem);
	}
	
	public void AddConnectible(Connectibles newConnectible) {
		SystemNode lastSystem = (SystemNode)SystemList.get(SystemList.size());
		lastSystem.AddConnectible(newConnectible);
	}
	
	public void AddConnection(Connection newConnection) {
		SystemNode lastSystem = (SystemNode)SystemList.get(SystemList.size());
		lastSystem.AddConnection(newConnection);
	}
	
	public void AddTo(to newTo) {
		SystemNode lastSystem = (SystemNode)SystemList.get(SystemList.size());
		lastSystem.AddTo(newTo);
	}
	
	public void AddOrgan(Organ newOrgan) {
		OrganList.add(newOrgan);
	}
	
	/*public void AddOrgan( newSystem) {
		SystemList.add(newSystem);
	}*/
	

	@Override
	public String WriteToJson() {
		String Json = "";
		for (Node node : SystemList) 
		{
			Json +=  node.WriteToJson();
		}
		
		for (Node node : OrganList) 
		{
			Json += node.WriteToJson();
		}
		
		return Json;
	}

	@Override
	public String WriteToXML() {
		// TODO Auto-generated method stub
		return null;
	}

}
